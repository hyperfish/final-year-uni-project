import paho.mqtt.client as mqtt
import time
import json

def on_message(client, userdata, message):
    stuff = message.payload.decode("utf-8")
    message_decode = json.loads(stuff)
    # print("received message: ", str(message.payload.decode("utf-8")))
    print("date: ", message_decode["date"], "uid: ", message_decode["UID"], "people_count: ", message_decode["people_count"])
    # print(json.loads(message))
    

def on_connect(client, userdata, flags, rc):
    print("receiver connected")
    client.subscribe("UOM/Toilet/Camera")

def on_disconnect(client, userdata, rc):
    print("Disconnected result code "+str(rc))
    client.loop_stop()

# mqttBroker = "mqtt.eclipse.org"
mqttBroker = "broker.emqx.io"

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

client.connect(mqttBroker, 1883)

# client.loop_start()

# time.sleep(30)
# client.loop_stop()
client.loop_forever()