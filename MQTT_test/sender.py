import paho.mqtt.client as mqtt 
from random import randrange, uniform
import time
import json

# mqttBroker ="mqtt.eclipse.org" 
mqttBroker = "broker.emqx.io"

randoNum = 0.0
MQTT_MSG = json.dumps({"date": "19.23.45","UID":  "living_room_1","people_count": str(randoNum)})

# Define on_publish event function
def on_publish(client, userdata, mid):
    print("Message Published...")
    # client.publish("PEOPLE", MQTT_MSG)
    # time.sleep(5)

def on_connect(client, userdata, flags, rc):
    print("Sender connected rc: " + str(rc))
    # client.publish("PEOPLE", MQTT_MSG)

def on_disconnect(client, userdata, rc):
    print("Disconnected result code "+str(rc))
    client.loop_stop()

def device_updates():
    print('updating number')
    global randoNum
    global MQTT_MSG
    randoNum = uniform(20.0, 21.0)
    MQTT_MSG = json.dumps({"date": "19.23.45","UID":  "living_room_1","people_count": randoNum})
    print('updated to ' + str(randoNum))

def mqtt_loop():
    device_updates()
    print(MQTT_MSG)
    client.publish("UOM/Toilet/Camera", MQTT_MSG)
    time.sleep(5)
    mqtt_loop()

client = mqtt.Client()
client.on_connect = on_connect
client.on_publish = on_publish
client.on_disconnect = on_disconnect
client.connect(mqttBroker, 1883, 45) 

# while True:
#     randNumber = uniform(20.0, 21.0)
#     payload = ()
#     client.publish("PEOPLE", randNumber)
#     print("Just published " + str(randNumber) + " to topic TEMPERATURE")
#     time.sleep(1)
client.loop_start()
mqtt_loop()
# while True:
#     client.publish("PEOPLE", MQTT_MSG)
#     device_updates()
#     # MQTT_MSG = json.dumps({"date": "19.23.45","UID":  "living_room_1","people_count": randoNum})
#     time.sleep(5)