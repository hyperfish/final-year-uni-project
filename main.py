# Heatmap code taken from https://github.com/plotly/dash-sample-apps/tree/master/apps/dash-clinical-analytics MIT licence
# Big thanks to Steve's internet guide for MQTT http://www.steves-internet-guide.com/mqtt/

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
from collections import deque
import plotly.figure_factory as ff

import numpy as np
import math

from twilio.rest import Client

import paho.mqtt.client as mqtt
from time import sleep

import datetime
from datetime import datetime as dt

import flask
import pandas as pd
import time
import os
import json
import random

# Twilio stuff testing
account_sid = 'AC9410671fcba526802a6cc0891a151c6c'
auth_token = 'b853d8f38152b0c4f5685df34e90a384'
twi_client = Client(account_sid, auth_token)
from_number = 'whatsapp:+14155238886'
to_number = 'whatsapp:+447746306777'

chart_type = True # True for line, False for bar

updates_list = []

server = flask.Flask('app')
server.secret_key = os.environ.get('secret_key', 'secret')

# file for list of cameras and IDs
cameraIDs = pd.read_csv('./data_folder/camera_IDs.csv', index_col=0, squeeze=True).to_dict() 

app = dash.Dash(__name__, server=server)

app.scripts.config.serve_locally = False
dcc._js_dist[0]['external_url'] = 'https://cdn.plot.ly/plotly-basic-latest.min.js'

app.title = 'People Counter Dashboard'
app.layout = html.Div(children=[
    # Main graph
    html.Div(className='pretty_container', children=[
        html.H1('People counter'),
        # html.Button('Button', id='chart-toggle', n_clicks=0),
        dcc.Dropdown(
            id='my-dropdown',
            className='people-counter-dropdown',
            options=[{'label': k, 'value': v} for k,v in cameraIDs.items()],
            value=next(iter(cameraIDs.values()))
        ),
        dcc.Graph(id='my-graph'),
        # interval in milliseconds
        dcc.Interval(id='graph-update', interval=1 * 1000, n_intervals = -1)
    ]),

    # Heatmap
    html.Div(className='pretty_container', children=[
        html.H1(children='HEATMAP'),
        html.P("Select date range"),
        dcc.DatePickerRange(
            id="date-picker-select",
            start_date=dt(2021, 3, 1),
            end_date=dt(2021, 3, 11),
            min_date_allowed=dt(2018, 1, 1),
            max_date_allowed=dt(2030, 12, 31),
            initial_visible_month=dt(2021, 3, 1),
        ),
        html.P(id="heatmap-title"),
        dcc.Graph(id='heatmap'),
        dcc.Interval(id='heatmap-update', interval=60 * 1000)
    ]),

    # Announcements
    html.Div(className='pretty_container', children=[
        html.H1('ANNOUNCEMENTS'),
        # html.Button('Button', id='button-click', n_clicks=0),
        dcc.Markdown('The stuff\n', 
                    style={'white-space':'pre', 
                            'overflow-x':'auto', 
                            'height':'150px', 
                            'width':'600px', 
                            'outline':'2px solid black'
                        }, 
                    id='announcement'
                    ),
        dcc.Interval(id='announcement-update', interval=5 * 1000)

   ])
])

@app.callback(Output('chart-toggle', 'children'),
    [Input('chart-toggle', 'n_clicks')])

def update_chart_type(n):
    global chart_type
    if n > 0:
        chart_type = not chart_type
    print(chart_type)


# GRAPH
@app.callback(Output('my-graph', 'figure'),
              [Input('my-dropdown', 'value'),
               Input('graph-update', 'n_intervals')])

def update_graph(selected_dropdown_value, n):
    # print("the datafrane:", latest_count)
    # print("graph...", n)
    df = pd.read_csv('./data_folder/data.csv')

    dff = df[df['ID'] == selected_dropdown_value]
    # if chart_type == True:
    fig = px.line(dff, x="Date", y="Count", title="People count for %s"%(selected_dropdown_value)).update_traces(mode='lines')
    # fig.add_bar(x=dff["Date"], y=dff["Count"])
    # else:
    # #     fig = px.line(dff, x="Date", y="Count", title="People count for %s"%(selected_dropdown_value))
    #     fig = px.bar(dff, x="Date", y="Count", title="People count for %s"%(selected_dropdown_value))
    #     fig.update_layout(bargap=0.01)
    # fig = px.scatter(dff, x="Date", y="Count", title="People count").update_traces(mode='lines+markers')
    fig.update_xaxes(
        rangeslider_visible=True,
        rangeselector=dict(
            buttons=list([
                dict(count=1, label="1m", step="month", stepmode="backward"),
                dict(count=7, label="1wk", step="day", stepmode="backward"),
                dict(count=6, label="6m", step="month", stepmode="backward"),
                dict(count=1, label="YTD", step="year", stepmode="todate"),
                dict(count=1, label="1y", step="year", stepmode="backward"),
                dict(step="all")
            ])
        )
    )
    fig.update_layout(uirevision=True) # TO SAVE STATE OF GRAPH, WONT RESET VIEW AFTER LIVE UPDATE
    # fig.update_layout(xaxis_rangeslider_visible=True)
    return fig
    # return {
    #     'data': [{
    #         'x': dff.Date,
    #         'y': dff.Count,
    #         'line': {
    #             'width': 3,
    #             'shape': 'linear'
    #         }
    #     }],
    #     'layout': {
    #         'margin': {
    #             'l': 30,
    #             'r': 20,
    #             'b': 30,
    #             't': 20
    #         },
    #         'uirevision':'true'
    #     }
    # }

def generate_heatmap(start, end, drop_down):
    df = pd.read_csv('./data_folder/data.csv') 
    df["Date"] = df["Date"].apply(
    lambda x: dt.strptime(x, "%Y-%m-%d %H:%M:%S")
    )  # String -> Datetime

    # Insert weekday and hour of checkin time
    df["Days of Wk"] = df["Hour"] = df["Date"]
    df["Days of Wk"] = df["Days of Wk"].apply(
        lambda x: dt.strftime(x, "%A")
    )  # Datetime -> weekday string

    df["Hour"] = df["Hour"].apply(
        # lambda x: dt.strftime(x, "%I %p")
        lambda x: dt.strftime(x, "%H")
    )  # Datetime -> int(hour) + AM/PM

    day_list = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ]

    filtered_df = df[(df["ID"] == drop_down)]
    filtered_df = filtered_df.sort_values("Date").set_index("Date")[start:end]

    x_axis = [datetime.time(i).strftime("%H") for i in range(24)]  # 24hr time list
    y_axis = day_list

    hour_of_day = ""
    weekday = ""
    shapes = []

    z = np.zeros((7, 24))
    annotations = []

    for ind_y, day in enumerate(y_axis):
        filtered_day = filtered_df[filtered_df["Days of Wk"] == day]
        for ind_x, x_val in enumerate(x_axis):
            sum_of_record = filtered_day[filtered_day["Hour"] == x_val][
                "Count"
            ].max()
            if math.isnan(sum_of_record):
                sum_of_record = 0
            z[ind_y][ind_x] = sum_of_record
            # print(sum_of_record)

            annotation_dict = dict(
                showarrow=False,
                text="<b>" + str(sum_of_record) + "<b>",
                xref="x",
                yref="y",
                x=x_val,
                y=day,
                font=dict(family="sans-serif"),
            )
            # Highlight annotation text by self-click
            if x_val == hour_of_day and day == weekday:
                if not reset:
                    annotation_dict.update(size=15, font=dict(color="#ff6347"))

            annotations.append(annotation_dict)

    # Heatmap
    hovertemplate = "<b> %{y}  Hour: %{x} <br><br> People Count: %{z}"

    data = [
        dict(
            x=x_axis,
            y=y_axis,
            z=z,
            type="heatmap",
            name="",
            hovertemplate=hovertemplate,
            showscale=False,
            colorscale=[[0, "#caf3ff"], [1, "#2c82ff"]],
        )
    ]

    layout = dict(
        margin=dict(l=70, b=50, t=50, r=50),
        modebar={"orientation": "v"},
        font=dict(family="Open Sans"),
        annotations=annotations,
        shapes=shapes,
        xaxis=dict(
            side="top",
            ticks="",
            ticklen=2,
            tickfont=dict(family="sans-serif"),
            tickcolor="#ffffff",
            title="Hours",
        ),
        yaxis=dict(
            side="left", ticks="", tickfont=dict(family="sans-serif"), ticksuffix=" ", title="Day",
        ),
        hovermode="closest",
        showlegend=False,
    )
    return {"data": data, "layout": layout}

# Date picker
@app.callback(
    Output("heatmap", "figure"),
    [
        Input("date-picker-select", "start_date"),
        Input("date-picker-select", "end_date"),
        Input('my-dropdown', 'value'),
        Input('heatmap-update', 'n_intervals')
    ],
)

def update_heatmap(start, end, dropdown_value, n_intervals):  
    # print('heatmap...')
    return generate_heatmap(
        start, end, dropdown_value
    )

# heatmap title
@app.callback(
    Output("heatmap-title", "children"),
    [Input('my-dropdown', 'value'),],
)

def update_heatmap_title(drop_down):
    return [html.H3("Current selected camera: %s"%(drop_down))]

# ANNOUNCEMENTS
@app.callback(Output('announcement', 'children'),
    [Input('announcement-update', 'n_intervals')],
    [State('announcement', 'children')]
)

def update_announcement(n, value):
    # value = value + 'New stuff\n'
    # print('announcements...')
    # file for list of announcements
    updates_list = open('./data_folder/updates.txt', 'r').readlines()
    announce = ''
    for line in updates_list:
        announce = announce + line

    with open('./data_folder/updates.txt', 'w') as f:
        for line in updates_list:
            f.write('%s' % line)

    return announce

# FOR TESTING, UPDATES ANNOUNCEMENT
@app.callback(Output('button-click', 'children'),
    [Input('button-click', 'n_clicks')])

def updates_ann_list(n):
    if n > 0:
        # updates_list.append('More stuff {}\n'.format(n))
        message = twi_client.messages.create(body='(Sidewalk Cam 3) High count (9) on 2021-03-06 16:42:43', from_=from_number, to=to_number)
        message = twi_client.messages.create(body='(%s) High count (%d) on %s'%(), from_=from_number, to=to_number)
        print(message.sid)

def on_connect(client, userdata, flags, rc):
    # print('CONNECTED')
    if rc == 0:
        print("receiver connected")
        client.subscribe([
                            ("UOM/Toilet/Camera",1), 
                            ("UOM/Outside/Camera",1)
                        ])
    else:
        print("failed to connect, code: ", rc)

def on_message(client, userdata, message):
    print("message:",str(message))
    try:
        message_decode = json.loads(message.payload.decode("utf-8"))
    except ValueError as e:
        print('Not a JSON!!')
        return False

    time.sleep(10)
    theDataFrame = pd.DataFrame([[message_decode["date"],message_decode["people_count"],message_decode["UID"]]], columns=list('ABC'))
    theDataFrame.to_csv('./data_folder/data.csv', mode='a', header=False, index=False)
    if message_decode["people_count"] > 5:
        print('HIT HEREEEEEEE')
        ann = "("+str(message_decode["UID"])+") "+"High number of people "+"("+str(message_decode["people_count"])+") "+"on "+str(message_decode["date"])+"\n"
        print('ann:', ann)
        with open('./data_folder/updates.txt', 'a') as f:
            f.write(str(ann))
        # updates_list.append(ann)
        message = twi_client.messages.create(body=ann, from_=from_number, to=to_number)

def on_disconnect(client, userdata, rc):
    print("disconnected, code: ", rc)

client = mqtt.Client("client1hometest")
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message
client.username_pw_set(username="mqtt",password="password")
client.connect("broker.emqx.io", 1883, 45)
# client.connect("localhost", 1883, 45)
client.loop_start()

if __name__ == '__main__':
    app.run_server()



