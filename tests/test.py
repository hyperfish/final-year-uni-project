import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# driver = webdriver.Chrome('../chromedriver/chromedriver')  # Optional argument, if not specified will search path.
# driver.get('http://127.0.0.1:8050/')
# time.sleep(2) # Let the user actually see something!
# search_box = driver.find_element_by_id('my-dropdown')
# # search_box.send_keys('appl stock')
# search_box.click()
# # search_box.submit()
# time.sleep(5) # Let the user actually see something!
# driver.quit()

class PythonOrgSearch(unittest.TestCase):

  def setUp(self):
    self.driver = webdriver.Chrome('../chromedriver/chromedriver')
    self.driver.get('http://127.0.0.1:8050/')

  def test_search_in_python_org(self):
    print('Testing page title')
    self.assertIn("People Counter Dashboard", self.driver.title)

  # test that selecting school_5 on dropdown menu works
  def test_dropdown_menu_school(self):
    # lazy method to wait for page to load fully
    time.sleep(6)
    print('Testing drop menu school 5')
    dropdown_box = self.driver.find_element_by_xpath("""//*[@id="my-dropdown"]/div/div/span[2]""")
    action = webdriver.common.action_chains.ActionChains(self.driver)
    action.move_to_element_with_offset(dropdown_box, 1, 1).click().send_keys(Keys.RETURN).perform()
    time.sleep(2) # sleep to allow actions to process
    graph_title = self.driver.find_element_by_css_selector("""#my-graph > div.js-plotly-plot > div > div > svg:nth-child(3) > g.infolayer > g.g-gtitle > text""")
    assert graph_title.text == 'People count for School_5'

  # test that selecting home_1 on dropdown menu works
  def test_dropdown_menu_home(self):
    # lazy method to wait for page to load fully
    time.sleep(6)
    print('Testing drop menu home 1')
    dropdown_box = self.driver.find_element_by_xpath("""//*[@id="my-dropdown"]/div/div/span[2]""")
    action = webdriver.common.action_chains.ActionChains(self.driver)
    action.move_to_element_with_offset(dropdown_box, 1, 1).click().send_keys(Keys.DOWN).send_keys(Keys.RETURN).perform()
    time.sleep(2) # sleep to allow actions to process
    graph_title = self.driver.find_element_by_css_selector("""#my-graph > div.js-plotly-plot > div > div > svg:nth-child(3) > g.infolayer > g.g-gtitle > text""")
    assert graph_title.text == 'People count for Home_1'

  def tearDown(self):
    self.driver.close()

if __name__ == "__main__":
    unittest.main()