Final Year Project UOM

REQUIREMENTS:
sender/receiver
- pip install paho-mqtt==1.5.1

Dashboard:
- pip install dash==1.16.3
- pip install Flask==1.1.2
- pip install pandas==1.1.3
- pip install plotly==4.12.0
- pip install twilio==6.53.0

Testing:
- pip install pytest-dash==2.1.2 | - "pip install dash[testing]"

MQTT:
- Make sure mosquitto broker installed
- Set username-password in [mosquitto.conf] with "allow_anonymous false" and "password_file [path to file]"

Running:
"python main.py"

Run test:
"python test.py"

Run mosquitto broker:
"net start mosquitto"

Run mosquitto password encrypter
"mosquitto_passwd -U [file path]"