import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
from collections import deque

# import paho.mqtt.client as mqtt
from time import sleep

import flask
import pandas as pd
import time
import os

MAX_QUEUE = deque(maxlen=20)

server = flask.Flask('app')
server.secret_key = os.environ.get('secret_key', 'secret')

# df = pd.read_csv('./data_folder/test.csv')
df = pd.read_csv('./data_folder/data.csv')
# cameraIDs = pd.read_csv('./data_folder/camera_IDs.csv', header=None, dtype={str: str}).set_index(0).squeeze().to_dict()
cameraIDs = pd.read_csv('./data_folder/camera_IDs.csv', index_col=0, squeeze=True).to_dict()

app = dash.Dash(__name__, server=server)

app.scripts.config.serve_locally = False
dcc._js_dist[0]['external_url'] = 'https://cdn.plot.ly/plotly-basic-latest.min.js'

app.layout = html.Div([
    html.H1('People counter'),
    dcc.Dropdown(
        id='my-dropdown',
        # options=[
        #     {'label': 'Living_room_1', 'value': 'TSLA'},
        #     {'label': 'Living_room_2', 'value': 'AAPL'},
        #     {'label': 'Classroom_1', 'value': 'COKE'}
        # ],
        # value='TSLA'
        # options=[
        #     {'label': 'School Camera 5', 'value': 'School_5'},
        #     {'label': 'Home Camera 1', 'value': 'Home_1'},
        # ],
        # value='Home_1'
        options=[{'label': k, 'value': v} for k,v in cameraIDs.items()],
        value=''
    ),
    dcc.Graph(id='my-graph'),
    dcc.Interval(id='graph-update', interval=1000)
], className="container")

# @app.callback(Output('my-graph', 'figure'),
#               events=[Event('graph-update', 'interval')],
#               [Input('my-dropdown', 'value')])

@app.callback(Output('my-graph', 'figure'),
              [Input('my-dropdown', 'value')])

# def update_graph(selected_dropdown_value):
#     dff = df[df['Stock'] == selected_dropdown_value]
#     print('THE STUFF', df)
#     return {
#         'data': [{
#             'x': dff.Date,
#             'y': dff.Close,
#             'line': {
#                 'width': 3,
#                 'shape': 'spline'
#             }
#         }],
#         'layout': {
#             'margin': {
#                 'l': 30,
#                 'r': 20,
#                 'b': 30,
#                 't': 20
#             }
#         }
#     }

def update_graph(selected_dropdown_value):
    dff = df[df['ID'] == selected_dropdown_value]
    return {
        'data': [{
            'x': dff.Date,
            'y': dff.Count,
            'line': {
                'width': 3,
                'shape': 'spline'
            }
        }],
        'layout': {
            'margin': {
                'l': 30,
                'r': 20,
                'b': 30,
                't': 20
            }
        }
    }

if __name__ == '__main__':
    app.run_server()

# def on_connect(client, userdata, flags, rc):
#     if rc == 0:
#         print("connected successfully")
#     else:
#         print("failed to connect, code: ", rc)
#   # client.subscribe()

# def on_disconnect(client, userdata, flags, rc):
#     print("disconnected, code: ", rc)

# client = mqtt.Client("client1")
# client.on_connect = on_connect
# client.on_disconnect = on_disconnect